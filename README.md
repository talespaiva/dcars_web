# DCARS_WEB

A website for visualizing workouts.

A `.env` file must exist at the root directory. Example:

```ini
[Postgres]
db_name = dcars
user = <username>
password = <password>
host = localhost
port = 5432
```
