from django.contrib.gis.db import models


class City(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    country_code = models.CharField(max_length=5, blank=True, null=True)

    def __str__(self):
        return str(self.name) if self.name else ''

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
        managed = False
        db_table = 'city'


class User(models.Model):
    id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=100)
    display_name = models.CharField(max_length=100, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    locality = models.CharField(max_length=100, blank=True, null=True)
    region = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return str(self.id) if self.id else ''

    class Meta:
        managed = False
        db_table = 'user'


class Workout(models.Model):
    id = models.BigIntegerField(primary_key=True)
    id_user = models.ForeignKey(User, models.DO_NOTHING, db_column='id_user')
    id_city = models.ForeignKey(City, models.DO_NOTHING, db_column='id_city')
    start_datetime = models.DateTimeField()
    activity_id = models.IntegerField()
    subactivity_id = models.IntegerField(blank=True, null=True)
    distance = models.FloatField(blank=True, null=True)
    mmf_distance = models.FloatField(blank=True, null=True)
    mmf_duration = models.FloatField(blank=True, null=True)
    source = models.CharField(max_length=100, blank=True, null=True)
    start_address = models.CharField(max_length=10000, blank=True, null=True)
    trajectory = models.LineStringField()
    raw_trajectory = models.LineStringField()
    diff_length = models.BigIntegerField(blank=True, null=True)

    def __str__(self):
        return str(self.id) if self.id else ''

    class Meta:
        managed = False
        db_table = 'workout'
