from django.conf.urls import url
from .views import user_detail, user_list, workout_detail

urlpatterns = [
    url(r'^user/$', user_list),
    url(r'^user/(?P<pk>[0-9]+)/$', user_detail),
    url(r'^workout/(?P<pk>[0-9]+)/$', workout_detail),
]
