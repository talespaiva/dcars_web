#from django.contrib import admin
from django.contrib.gis import admin
from .models import Workout, User, City

admin.site.register(User)
admin.site.register(Workout, admin.OSMGeoAdmin)
admin.site.register(City)
