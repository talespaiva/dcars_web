from rest_framework import serializers
from .models import User, Workout, City


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name', 'state', 'country', 'country_code')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'display_name', 'gender', 'country',
                  'locality', 'region')


class WorkoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workout
        fields = ('id', 'id_user', 'id_city', 'start_datetime', 'activity_id',
                  'subactivity_id', 'mmf_distance', 'distance', 'mmf_duration',
                  'source', 'start_address', 'trajectory', 'raw_trajectory',
                  'diff_length')
