from django.conf.urls import url
from dcars_web.core import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
]
